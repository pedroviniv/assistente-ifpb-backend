/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.assistente.ifpb.backend.model.vo;

import java.io.Serializable;
import org.mongodb.morphia.annotations.Embedded;

/**
 *
 * @author PedroArthur
 */

@Embedded
public class Name implements Serializable {
    
    private String firstname;
    private String lastname;

    public Name(String firstname, String lastname) {
        this.firstname  = firstname;
        this.lastname   = lastname;
    }

    public Name() {
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    @Override
    public String toString() {
        return "Name{" + "firstname=" + firstname +
                ", lastname=" + lastname + '}';
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.assistente.ifpb.backend.repository;

import io.github.kieckegard.assistente.ifpb.backend.model.Assistent;
import javax.inject.Inject;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Key;

/**
 *
 * @author PedroArthur
 */
public class AssistentRepositoryMorphiaImpl implements AssistentRepository {
    
    private final Datastore datastore;

    @Inject
    public AssistentRepositoryMorphiaImpl(Datastore datastore) {
        this.datastore = datastore;
    }

    @Override
    public void persist(Assistent assistent) {
        Key<Assistent> key = this.datastore.save(assistent);
    }
    
}

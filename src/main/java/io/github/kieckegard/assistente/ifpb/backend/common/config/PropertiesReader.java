/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.assistente.ifpb.backend.common.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import javax.enterprise.context.ApplicationScoped;

/**
 *
 * @author PedroArthur
 */
@ApplicationScoped
public class PropertiesReader {

    public Properties read(String resourceName) throws IOException {

        InputStream is = PropertiesReader.class.getClassLoader()
                .getResourceAsStream(resourceName);

        Properties props = new Properties();
        props.load(is);

        return props;
    }
}

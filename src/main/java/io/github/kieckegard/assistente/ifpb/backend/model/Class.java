/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.assistente.ifpb.backend.model;

import java.io.Serializable;
import org.mongodb.morphia.annotations.Embedded;

/**
 *
 * @author PedroArthur
 */
@Embedded
public class Class implements Serializable {
    
    private String       cod;
    @Embedded
    private Discipline   discipline;
    private String       startsAt;
    private String       endsAt;

    public Class(String cod, Discipline discipline, 
            String startsAt, String endsAt) {
        
        this.cod         = cod;
        this.discipline  = discipline;
        this.startsAt    = startsAt;
        this.endsAt      = endsAt;
    }

    public Class() {
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public Discipline getDiscipline() {
        return discipline;
    }

    public void setDiscipline(Discipline discipline) {
        this.discipline = discipline;
    }

    public String getStartsAt() {
        return startsAt;
    }

    public void setStartsAt(String startsAt) {
        this.startsAt = startsAt;
    }

    public String getEndsAt() {
        return endsAt;
    }

    public void setEndsAt(String endsAt) {
        this.endsAt = endsAt;
    }

    @Override
    public String toString() {
        return "Class{" + "cod=" + cod + ", discipline=" + discipline +
                ", startsAt=" + startsAt + ", endsAt=" + endsAt + '}';
    }
}

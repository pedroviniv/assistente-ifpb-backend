/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.assistente.ifpb.backend.webservice.v1.endpoints;

import io.github.kieckegard.assistente.ifpb.backend.common.config.Configuration;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author PedroArthur
 */

@Path("hello")
public class HelloWorldEndPoint {
    
    private final Configuration configuration;
    private static final Logger LOG = Logger
            .getLogger(HelloWorldEndPoint.class.getName());
    
    @Inject
    public HelloWorldEndPoint(Configuration configuration) {
        this.configuration = configuration;
    }
    
    @GET
    @Path(value = "{name}")
    @Produces(MediaType.TEXT_PLAIN)
    public Response hello(@PathParam("name") @DefaultValue("world") String name) {
        
        String dbName = configuration.getProperty("morphia.db.name");
        LOG.log(Level.INFO, "morphia.db.name: {0}", dbName);
        
        String response = String.format("Hello, %s!", name);
        
        return Response.ok(response).build();
    }
    
}

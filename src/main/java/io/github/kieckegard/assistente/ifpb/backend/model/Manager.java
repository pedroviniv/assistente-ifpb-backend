/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.assistente.ifpb.backend.model;

import io.github.kieckegard.assistente.ifpb.backend.model.vo.Name;
import java.io.Serializable;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Reference;

/**
 *
 * @author PedroArthur
 */

@Entity("managers")
public class Manager implements Serializable {
    
    @Id
    private String  email;
    private String  cpf;
    @Reference
    private User    user;
    @Embedded
    private Name    name;

    public Manager(String cpf, User user, Name name, String email) {
        this.cpf    = cpf;
        this.user   = user;
        this.name   = name;
        this.email  = email;
    }

    public Manager() {
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Manager{" + "cpf=" + cpf + ", user=" + user +
                ", name=" + name + ", email=" + email + '}';
    }
}

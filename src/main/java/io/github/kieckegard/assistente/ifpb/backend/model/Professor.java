/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.assistente.ifpb.backend.model;

import java.io.Serializable;
import org.mongodb.morphia.annotations.Embedded;

/**
 *
 * @author PedroArthur
 */

@Embedded
public class Professor implements Serializable {
    
    private String abreviation;

    public Professor(String abreviation) {
        this.abreviation = abreviation;
    }

    public Professor() {
    }

    public String getAbreviation() {
        return abreviation;
    }

    public void setAbreviation(String abreviation) {
        this.abreviation = abreviation;
    }

    @Override
    public String toString() {
        return "Professor{" + "abreviation=" + abreviation + '}';
    }
}

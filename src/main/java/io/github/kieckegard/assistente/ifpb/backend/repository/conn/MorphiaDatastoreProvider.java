/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.assistente.ifpb.backend.repository.conn;

import com.mongodb.MongoClient;
import io.github.kieckegard.assistente.ifpb.backend.common.config.Configuration;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Default;
import javax.enterprise.inject.Produces;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

/**
 * An application scoped datastore factory implemented with CDI.
 * The factory method injects the mongo client provided by <code>MongoClientProvider</code>
 * and the configuration provided by <code>ConfigurationProvider</code>.
 * The Datastore here is served as a unique instance.
 * @author PedroArthur
 */
@ApplicationScoped
public class MorphiaDatastoreProvider {

    @Produces
    @ApplicationScoped
    @Default
    public Datastore getMongoDBConnection(MongoClient mongoClient,
            Configuration configuration) {

        final Morphia morphia = new Morphia();
        morphia.mapPackage("io.github.kieckegard.assistente.ifpb.backend.model");

        String dbName = configuration.getProperty("morphia.db.name");
        Datastore datastore = morphia.createDatastore(mongoClient, dbName);

        return datastore;
    }

}

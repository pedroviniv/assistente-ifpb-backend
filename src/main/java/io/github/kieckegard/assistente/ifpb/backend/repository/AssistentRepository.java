/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.assistente.ifpb.backend.repository;

import io.github.kieckegard.assistente.ifpb.backend.model.Assistent;

/**
 *
 * @author PedroArthur
 */
public interface AssistentRepository {
    
    void persist(Assistent assistent);
}

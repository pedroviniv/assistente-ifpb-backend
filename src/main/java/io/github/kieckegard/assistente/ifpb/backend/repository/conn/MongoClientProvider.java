/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.assistente.ifpb.backend.repository.conn;

import com.mongodb.MongoClient;
import io.github.kieckegard.assistente.ifpb.backend.common.config.Configuration;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

/**
 * An application scoped MongoClient factory implemented with CDI.
 * MongoClient can be singleton because the client is internally pooled and thread
 * safe.
 * @author PedroArthur
 */
@ApplicationScoped
public class MongoClientProvider {
    
    @ApplicationScoped
    @Produces
    public MongoClient provideMongoClient(Configuration configuration) {
        
        String host = configuration
                .getProperty("morphia.db.host");
        int port = Integer.parseInt(configuration
                .getProperty("morphia.db.port"));
        
        return new MongoClient(host, port);
    }
}

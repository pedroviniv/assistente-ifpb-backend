/*
 * To change this license header, choose License Headers in Project Configuration.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.assistente.ifpb.backend.common.config;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author PedroArthur
 */
public class Configuration implements Serializable {
    
    private final Map<String, String> properties;
    
    public Configuration() {
        this.properties = new HashMap<>();
    }

    public Configuration(Map<String, String> properties) {
        this.properties = properties;
    }
    
    public Configuration put(String key, String value) {
        this.properties.put(key, value);
        return this;
    }
    
    public String getProperty(String key) {
        return this.properties.get(key);
    }

    @Override
    public String toString() {
        return "Configuration{" + "properties=" + properties + '}';
    }
}

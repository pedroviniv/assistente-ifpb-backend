/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.assistente.ifpb.backend.common.config;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Default;
import javax.enterprise.inject.Produces;

/**
 * An application scoped configuration provider implemented with CDI.
 * This factory has the purpose to read a file named "config.properties"
 * create a Configuration instance with all the properties read from the file
 * and serve this instance as a singleton.
 * 
 * @author PedroArthur
 */
@ApplicationScoped
public class ConfigurationProvider {

    private static final Logger LOG = Logger
            .getLogger(ConfigurationProvider.class.getName());

    @ApplicationScoped
    @Produces
    @Default
    public Configuration provideConfiguration(PropertiesReader reader) {

        String propsFile = "config.properties";

        Configuration configuration = new Configuration();
        try {
            Properties props = reader.read(propsFile);
            Set<Entry<Object, Object>> entrySet = props.entrySet();
            Iterator<Entry<Object, Object>> iterator = entrySet.iterator();
            while (iterator.hasNext()) {
                Entry<Object, Object> entry = iterator.next();
                configuration.put(entry.getKey().toString(),
                        entry.getValue().toString());
            }
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, "Não foi possível ler o arquivo de propriedades "
                    + propsFile, ex);
        }

        return configuration;
    }
}

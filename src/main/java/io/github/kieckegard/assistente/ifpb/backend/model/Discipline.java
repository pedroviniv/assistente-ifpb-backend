/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.assistente.ifpb.backend.model;

import java.io.Serializable;
import org.mongodb.morphia.annotations.Embedded;

/**
 *
 * @author PedroArthur
 */

@Embedded
public class Discipline implements Serializable {
    
    private String        cod;
    private Course        course;
    private String        semester;
    private String        abreviation;
    private String        description;
    @Embedded
    private Professor     professor;

    public Discipline(String cod, Course course, String semester, 
            String abreviation, String description, Professor professor) {
        
        this.cod          = cod;
        this.course       = course;
        this.semester     = semester;
        this.abreviation  = abreviation;
        this.description  = description;
        this.professor    = professor;
    }

    public Discipline() {
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public String getAbreviation() {
        return abreviation;
    }

    public void setAbreviation(String abreviation) {
        this.abreviation = abreviation;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Professor getProfessor() {
        return professor;
    }

    public void setProfessor(Professor professor) {
        this.professor = professor;
    }

    @Override
    public String toString() {
        return "Discipline{" + "cod=" + cod + ", course=" + course +
                ", semester=" + semester + ", abreviation=" + abreviation +
                ", description=" + description + 
                ", professor=" + professor + '}';
    }
}

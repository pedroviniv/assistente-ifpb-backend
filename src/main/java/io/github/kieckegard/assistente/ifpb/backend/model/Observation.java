/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.assistente.ifpb.backend.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Reference;

/**
 *
 * @author PedroArthur
 */

@Entity("observations")
public class Observation implements Serializable {
    
    @Id
    private Long            id;
    private String          description;
    @Reference
    private List<Class>     classes;
    @Reference
    private Assistent       author;

    public Observation(Long id, String description, 
            List<Class> classes, Assistent author) {
        
        this.id           = id;
        this.description  = description;
        this.classes      = classes;
        this.author = author;
    }

    public Observation() {
        this.classes = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Class> getClasses() {
        return classes;
    }

    public void setClasses(List<Class> classes) {
        this.classes = classes;
    }

    public Assistent getAuthor() {
        return author;
    }

    public void setAuthor(Assistent author) {
        this.author = author;
    }

    @Override
    public String toString() {
        return "Observation{" + "id=" + id + ", description=" + description +
                ", classes=" + classes + ", author=" + author + '}';
    }
}

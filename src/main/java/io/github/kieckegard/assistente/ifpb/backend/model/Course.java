/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.assistente.ifpb.backend.model;

import java.io.Serializable;
import org.mongodb.morphia.annotations.Embedded;

/**
 *
 * @author PedroArthur
 */

@Embedded
public class Course implements Serializable {
    
    private String cod;
    private String abrev;
    private String description;

    public Course(String cod, String abrev, String description) {
        this.cod          = cod;
        this.abrev        = abrev;
        this.description  = description;
    }

    public Course() {
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public String getAbrev() {
        return abrev;
    }

    public void setAbrev(String abrev) {
        this.abrev = abrev;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Course{" + "cod=" + cod + ", abrev=" + abrev + 
                ", description=" + description + '}';
    }
}
